import React,{Component} from "react";
import './App.css';
import { Link } from "react-router-dom";
import uuid from "react-uuid"
import ListItem from "./ListItem";

class Additem extends Component {
  constructor(props){
    super(props);
      this.state = {
        items: [],
        userInput:{
          id:'',
          name:''
        },
        isEdit:false,
        isAddSubTask: false,
        parentid:'',
        isChildEdit:false,
      }
  }

  handleInput = (e) => {
    let { userInput } = this.state;
    userInput.name = e.target.value;
    this.setState({ userInput });
  }

  addItem = () => {
    let { items, userInput, isEdit, isAddSubTask, parentid, isChildEdit } = this.state;
    if(userInput.name !== ""){
      if(isAddSubTask){
        let index = items.findIndex(x => x.id === parentid);
        let data ={
          subid: uuid(),
          subname: userInput.name
        }
        items[index].subtask.push(data);
        this.setState({items: items, isAddSubTask: false}, () =>{
          this.clearItem();
        });
      }
      else if(isEdit){
        let index = items.findIndex(x => x.id === userInput.id);
        let updateItem = { id :userInput.id, name: userInput.name, subtask:[]};
        items[index] = updateItem;
        this.setState({ isEdit:false, items: items }, () => {
        this.clearItem();
        })
      }
      else if(isChildEdit){
        let index = items.findIndex(x => x.id === parentid);
        let childIndex = items[index].subtask.findIndex(x => x.subid === userInput.id);
        items[index].subtask[childIndex].subname = userInput.name;
        this.setState({items: items, isChildEdit: false}, () =>{
          this.clearItem();
        })
      }
      else {
        let data = {
          id: uuid(),
          name: userInput.name,
          subtask:[]
        }
        items.push(data);
        this.setState({ items }, () => {
        this.clearItem();
        });
      }
    }
  }

  clearItem = () => {
    let data = {
      id: '',
      name: ''
    }
    this.setState({ userInput: data });
  }

  deleteItem = (id) =>{
    let filterItem = this.state.items.filter(item =>{
      return item.id !== id;
    })
    this.setState({items: filterItem})
  }

  editItem = (item) =>{
    let data = {
      name: item.name,
      id:item.id
    }
    this.setState({isEdit: true, userInput: data })
  }

  addSubtask = (item) =>{
    this.setState({ isAddSubTask: true, parentid: item.id })
  }

  deleteChildItem = (id, item1) =>{
    let { items } = this.state;
    let index = items.findIndex(item => item.id === item1.id);
    let childIndex = items[index].subtask.findIndex(x => x.subid === id)
    items[index].subtask[childIndex] = [];
    this.setState({items});
  }

  editChildItems = (id, item) =>{
    let { items } = this.state;
    let index = items.findIndex(x => x.id === item.id);
    let childIndex = items[index].subtask.findIndex(x => x.subid === id);
    let data = {
      name: item.subtask[childIndex].subname,
      id: item.subtask[childIndex].subid
    }
    this.setState({isChildEdit: true, parentid:item.id, userInput:data})
  }

  displayForm = () =>{
    let { isEdit, isAddSubTask, isChildEdit } = this.state;
      return (
        <div id="to-do-form">
          <input type="text" onChange={this.handleInput} value={this.state.userInput.name}/>
          <button onClick={this.addItem} className="btn btn-success">{isChildEdit ? 'Save':isAddSubTask ? 'Add Subtask' : isEdit ? 'Edit Item' : 'Add Item' }</button>
          <ListItem items={this.state.items} deleteItem={this.deleteItem} editItem={this.editItem} addSubtask ={this.addSubtask} deleteChildItem={this.deleteChildItem} editChildItems={this.editChildItems}></ListItem>
        </div>
      )
  }

  render(){
    return (
      <div className="todo">
        {this.displayForm()}
      </div>
        )
    }
  }
export default Additem;
