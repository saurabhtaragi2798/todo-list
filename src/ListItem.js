import React,{ Component } from "react";
import Subtask from "./Subtask"
class ListItem extends Component {
  deleteItem = (id) =>{
    this.props.deleteItem(id)
  }

  editItem = (item) =>{
    this.props.editItem(item);
  }

  addSubtask = (item) =>{
    this.props.addSubtask(item)
  }
  deleteChildItem = (id, items) =>{
    this.props.deleteChildItem(id, items);
  }
  editChildItems = (id, items) =>{
    this.props.editChildItems(id, items);
  }

  displayItem = (item, index) =>{
      let view =
        <div className="p">
          {item.name}
          <span className="div-btn">
          <button className="btn btn-primary" onClick={()=>{this.editItem(item)}}>Edit</button>
          <button className="btn btn-info" onClick={()=>{this.addSubtask(item)}}>Add Subtask</button>
          <button className="btn btn-danger" onClick={()=>{this.deleteItem(item.id)}}>Delete</button>
          </span>
          {(item && item.subtask && item.subtask.length > 0 ) && <Subtask items={item} deleteChildItem={this.deleteChildItem} editChildItems={this.editChildItems}/>}
        </div>
    return view;

  }

  render(){
    return(
      <div>
        {
        (this.props.items) && Object.keys(this.props.items).map((item, index)=>{
          return(
            <div className="list" key={index}>{this.displayItem(this.props.items[item],index)}</div>
                )
              })
        }
      </div>
        )
  }
}

export default ListItem
