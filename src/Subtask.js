import React,{Component} from "react"


class Subtask extends Component {

  constructor(props){
    super(props);

    this.state = {
      isVisible :true
    }
  }

  deleteChildItem = (id ,items) =>{
    this.props.deleteChildItem(id, items);
    this.setState({ isVisible: false });
  }

  editChildItems = (id ,items) =>{
    this.props.editChildItems(id, items)
  }

  displayChildItem = (item) => {
    let view;
    if( this.state.isVisible ) {
      view =
      <React.Fragment>
        {item.subname}
        <button className="btn btn-primary" onClick={()=>{this.editChildItems(item.subid, this.props.items)}}>Edit</button>
        <button className="btn btn-danger" onClick={()=>{this.deleteChildItem(item.subid, this.props.items)}}>Delete</button>
      </React.Fragment>
    }
    return view
  }

  render(){
    return(
      <div>
        {this.props.items.subtask.map((item, index) =>{
          return(
              <div key={index} className="btn-div">
                {this.displayChildItem(item)}
              </div>
          )
        })}
      </div>
    )
  }
}
export default Subtask
